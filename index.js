const bodyParser = require('body-parser')
const express = require('express')
const logger = require('morgan')
const app = express()
const {
  fallbackHandler,
  notFoundHandler,
  genericErrorHandler,
  poweredByHandler
} = require('./handlers.js')

// global variable to keep track of our current moving direction

var currentdirection = 'up'

// global variables to keep track of board's max height and width
var boardmaxwidth = 11
var boardmaxheight = 11
// global variable of snake's current health

var currenthealth = 100

// global current head position?
var headposition = {
  x: 0,
  y: 0, 
}

// For deployment to Heroku, the port needs to be set using ENV, so
// we check for the port number in process.env
app.set('port', (process.env.PORT || 9001))

app.enable('verbose errors')

app.use(logger('dev'))
app.use(bodyParser.json())
app.use(poweredByHandler)

// --- SNAKE LOGIC GOES BELOW THIS LINE ---

// Handle POST request to '/start'
app.post('/start', (request, response) => {
  // NOTE: Do something here to start the game
  boardmaxwidth = request.body.board.width
  boardmaxheight = request.body.board.height
  // Response data
  const data = {
    color: '#FFD700',
    headType: "sand-worm",
    tailType: "round-bum"
  }

  return response.json(data)
})

// Handle POST request to '/move'
app.post('/move', (request, response) => {
  // NOTE: Do something here to generate your move
  //request our current health and update health global
  //request our current position
  /*

  //to begin with just check if the head is at an edge of the board and if so turn
  // case switch for our current direction where in each case we check our position and then determine if we should switch 1 of 2 directions or keep going

  Basic logic layout:

  Snake should keep going one of the 4 directions until it encounters an obstacle
  FSM Case thing until however we get the data suggests the next tile is bad
  In that case it should check the other two possible directions and then select one

  //
  */
  var direction = currentdirection;
  //headposition.x = request.body.you.body[0].x //request's first x position
  //headpositon.y = request.body.you.body[0].y //request's first y position
  currenthealth = request.body.you.health //update health

  switch (direction){
    case 'up':

      currentdirection = 'right'
      // if(headposition.y == 0){ //we've reached the top of the board
      //   currentdirection = 'right'
      // };
      //
      break;
      // check if up is clear if not check left and right and consider switching

    case 'down':

      currentdirection = 'left'
      // if(headposition.y == boardmaxheight){ //we've reached the bottom of the board
      //   currentdirection = 'left'
      // }; 
      break;
    // check if down is clear if not check left and right and consider switching
    case 'left':

      currentdirection = 'up'
      // if(headposition.x == 0){
      //   currentdirection = 'up'
      // };
      break;

    // check if left is clear if not check up and down and consider switching
    case 'right':

      currentdirection = 'down'
      // if(headpositio.x == boardmaxwidth){
      //   currentdirection = 'down'
      // };
      break;
    // check if right is clear if not check up and down and consider switching
    default:

  }


  // Response data
  const data = {
    move: currentdirection, // one of: ['up','down','left','right']
  }

  return response.json(data)
})

app.post('/end', (request, response) => {
  // NOTE: Any cleanup when a game is complete.
  return response.json({})
})

app.post('/ping', (request, response) => {
  // Used for checking if this snake is still alive.
  return response.json({});
})

// --- SNAKE LOGIC GOES ABOVE THIS LINE ---

app.use('*', fallbackHandler)
app.use(notFoundHandler)
app.use(genericErrorHandler)

app.listen(app.get('port'), () => {
  console.log('Server listening on port %s', app.get('port'))
})
